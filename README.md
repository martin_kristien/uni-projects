This repository contains several projects done by Martin Kristien as part of study toward MInf Informatics degree at The University of Edinburgh, Scotland, UK.

Due to the university's policies about publishing courseworks, the repository is password protected. The password should be distributed only among potential employers reviewing work of Martin Kristien for purposes of an employment position offer.

If you did not receive a password for the protected archive as part of a Cover Letter or otherwise, or your password does not work, or you would like to review the content of the archive, contact Martin Kristien at *martin.kristien@gmail.com* to request the password. Please, include your credentials with the request, as the author must ensure the password is distributed only to legitimate entities.
